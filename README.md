# Hello-World-OpenWrt
Build sample openwrt project with gitlab runner.

## Download
[![pipeline status](https://gitlab.com/xponenthub/hello-world-openwrt/badges/master/pipeline.svg)](https://gitlab.com/xponenthub/hello-world-openwrt/commits/master)

[Pipelines](/../pipelines) > Artifacts

## Environment
- Ubuntu 14.04 or 16.04 (tested) or 17.04.
- OpenWrt framework.

## Requirements
1. gawak
2. ccache

## Build
1. Download and unzip the SDK pacakge content from [Downloads](https://docs.labs.mediatek.com/resource/linkit-smart-7688/en/downloads) page.
2. Unzip and rename to SDK folder.
3. Put C package with source under ``/SDK/package`` folder.
4. Build.

```
$ sudo apt update && sudo apt upgrade
$ sudo apt-get --no-install-recommends install gawk
$ sudo apt install ccache
$ cd SDK
$ make package/helloworld/compile V=99
```

## Output
Find at ``./SDK/bin/ramips/packages/base/helloworld*.ipk``


## Install Gitlab Runner on Ubuntu
1. Install gitlab runner.
2. Set _shell_ as executor and true for untagged jobs when register runner.
3. Turn off shared runners under CI/CD settings.

```
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt-get install gitlab-runner
$ gitlab-runner register
$ gitlab-runner run
```

see https://docs.gitlab.com/runner/install/linux-repository.html


Remove gitlab runner config if you want to reset

```
$ sudo rm /etc/gitlab-runner/config.toml
$ sudo rm /home/ubuntu/.gitlab-runner/config.toml
```


## Uninstall Gitlab Runner
```
$ sudo apt-get purge gitlab-runner
```


## Problems

### Failed to apt-get update
Unconfirmed
```
$ sudo rm -rf /var/lib/apt/lists /var/cache/apt
$ sudo apt update
$ sudo apt clean
```

## References
- [Using OpenWrt SDK to Build C/C++ Programs by MediaTek Labs](https://docs.labs.mediatek.com/resource/linkit-smart-7688/en/tutorials/c-c++-programming/using-openwrt-sdk-to-build-c-c++-programs)
- https://forum.gitlab.com/t/gitlab-ci-uses-docker-executor-instead-of-shell/6449/3
